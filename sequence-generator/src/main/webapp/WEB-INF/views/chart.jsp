<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="resources" value="${pageContext.request.contextPath}/resources"/>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Index</title>
        <link href="${resources}/css/Chart.min.css" rel="stylesheet">
        <link href="${resources}/css/style.css" rel="stylesheet">
    </head>
    <body>
        <h2>Dispersion: ${dispersion}</h2>
        <h2>Average value: ${avg}</h2>
        <div style="width: 60%;">
            <canvas id="chartBar" width="500" height="300"></canvas>
        </div>
        <script src="${resources}/js/Chart.bundle.min.js"></script>
        <script>
            var barData = new Object();
            var labels = new Array();
            <c:forEach items="${chartData.keySet()}" var="label">
                labels.push("${label}");
            </c:forEach>
            var dataSets = new Array();
            var dataSet = new Object();
            dataSet.label = "Distribution chart";
            dataSet.backgroundColor = "#4ACAB4";
            var data = new Array();
            <c:forEach items="${chartData.values()}" var="value">
                data.push(${value});
            </c:forEach>
            dataSet.data = data;
            dataSets.push(dataSet);
            barData.labels = labels;
            barData.datasets = dataSets;
            var context = document.getElementById('chartBar').getContext('2d');
            var chart = new Chart(context, {
                type : "bar",
                data : barData,
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        </script>
    </body>
</html>