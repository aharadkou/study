package com.gsu.sequence;

import com.gsu.generator.SequenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
@PropertySource("classpath:sequence.properties")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class NormalSequence implements Sequence {

    @Autowired
    public NormalSequence(final SequenceGenerator sequenceGenerator,
                          @Value("${elementsCount}") final int elementsCount,
                          @Value("${mathExpectation}") final int mathExpectation,
                          @Value("${dispersion}") final int dispersion) {
        sequence = sequenceGenerator.getNormalSequence(elementsCount, mathExpectation, dispersion);
    }

    private final double[] sequence;

    public double getAvgValue() {
        return Arrays
                .stream(sequence)
                    .average()
                        .orElseThrow(() -> new SequenceException("Can't calculate avg value!"));
    }

    public double getDispersion() {
        final double avgValue = getAvgValue();
        return Arrays
                .stream(sequence)
                    .map(value -> value * value - avgValue * avgValue)
                        .sum() / sequence.length;
    }

    public Map<String, Long> getBarChart(final int intervalCount) {
        Arrays.sort(sequence);
        final double first = sequence[0];
        final double last = sequence[sequence.length - 1];
        final double intervalInc = (last - first) / intervalCount;
        double startInterval = first;
        double endInterval = first + intervalInc;

        Map<String, Long> intervalCountMap = new LinkedHashMap<>();
        while (endInterval <= last) {
            long elementsCount = getElementCountInRange(startInterval, endInterval);
            if (endInterval == last) {
                elementsCount++;
            }
            intervalCountMap.put(String.format("[%.3f -> %3f)", startInterval, endInterval), elementsCount);
            startInterval = endInterval;
            endInterval += intervalInc;
        }
        return intervalCountMap;
    }

    private long getElementCountInRange(final double from, final double to) {
        return Arrays.stream(sequence).filter(value -> value >= from && value < to).count();
    }

}
