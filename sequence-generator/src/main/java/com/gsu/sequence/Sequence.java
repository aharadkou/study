package com.gsu.sequence;

import java.util.Map;

public interface Sequence {

    double getAvgValue();

    double getDispersion();

    Map<String, Long> getBarChart(int intervalCount);

}
