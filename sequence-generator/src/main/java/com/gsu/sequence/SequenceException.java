package com.gsu.sequence;

public class SequenceException extends RuntimeException {

    public SequenceException(final String mes) {
        super(mes);
    }

}
