package com.gsu.controllers;

import com.gsu.sequence.Sequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SequenceController {

    private static final int INTERVAL_COUNT = 10;

    private final Sequence sequence;

    @Autowired
    public SequenceController(final Sequence sequence) {
        this.sequence = sequence;
    }

    @GetMapping("/chart")
    public String buildBarChart(final Model model) {
        model.addAttribute("dispersion", sequence.getDispersion());
        model.addAttribute("avg", sequence.getAvgValue());
        model.addAttribute("chartData", sequence.getBarChart(INTERVAL_COUNT));
        return "chart";
    }

}
