package com.gsu.generator;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Random;

@Component
@Scope("request")
public class NormalSequenceGenerator implements SequenceGenerator {

    private final Random random = new Random(System.currentTimeMillis());

    public double[] getNormalSequence(final int elementsCount,
                                      final int mathExpectation,
                                      final int dispersion) {
        final double[] sequence = new double[elementsCount];
        for(int i = 0; i < elementsCount; i++) {
            double dSum = 0;
            for(int j = 0; j < elementsCount; j++) {
                dSum = dSum + random.nextDouble();
            }
            sequence[i] = (dSum - elementsCount / 2.0) / Math.sqrt(dispersion) + mathExpectation;
        }

        Arrays.sort(sequence);
        for(int i = 1; i < elementsCount; i++) {
            sequence[i] = sequence[i] - sequence[i - 1];
        }
        return sequence;
    }


}
