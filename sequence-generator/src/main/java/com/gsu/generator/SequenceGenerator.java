package com.gsu.generator;

public interface SequenceGenerator {

    double[] getNormalSequence(int elementsCount, int mathExpectation, int dispersion);

}
