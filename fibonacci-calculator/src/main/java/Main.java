import calculator.FibonacciCalculator;

import java.math.BigInteger;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Runner.
 */
public final class Main {

    private static final int EXIT_CODE = -1;

    private Main() {

    }

    /**
     * Runner logic.
     * @param args command line args
     */
    public static void main(final String[] args) {
        FibonacciCalculator fibonacciCalculator = new FibonacciCalculator();
        try (final Scanner scanner = new Scanner(System.in)){
            while (true) {
                try {
                    System.out.print("Enter n-> ");
                    int n = scanner.nextInt();
                    if(n == EXIT_CODE) {
                        break;
                    }
                    BigInteger fibonacciNum = fibonacciCalculator.calculateFibonacci(n);
                    System.out.printf("Calculated Fibonacci number -> %d. "
                                        + "Enter next n or %d to close application."
                                            + System.lineSeparator(), fibonacciNum, EXIT_CODE);
                } catch (IllegalArgumentException ex) {
                    System.err.println(ex.getMessage());
                } catch (InputMismatchException ex) {
                    scanner.next();
                    System.err.println("Invalid n! Please, try again.");
                }
            }
        }
    }
}
