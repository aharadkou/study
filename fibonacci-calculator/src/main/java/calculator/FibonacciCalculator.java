package calculator;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Fibonacci calculator.
 */
public class FibonacciCalculator {

    private List<BigInteger> cache;

    /**
     * No args constructor.
     */
    public FibonacciCalculator() {
        cache = new ArrayList<>(Arrays.asList(BigInteger.ZERO, BigInteger.ONE));
    }

    /**
     * Returns number with certain position in Fibonacci sequence.
     * @param n position in Fibonacci sequence
     * @return number with certain position in Fibonacci sequence
     * @throws IllegalArgumentException if position in Fibonacci sequence less than 0
     */
    public BigInteger calculateFibonacci(final int n) {
        if(n < 0) {
            throw new IllegalArgumentException("n mustn't be less than 0!");
        }
        if(n >= cache.size()) {
            for (int i = cache.size(); i <= n; i++) {
                BigInteger prePrev = cache.get(i - 2);
                BigInteger prev = cache.get(i - 1);
                cache.add(prePrev.add(prev));
            }
        }
        return cache.get(n);
    }


}
