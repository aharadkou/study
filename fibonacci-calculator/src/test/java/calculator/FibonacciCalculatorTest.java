package calculator;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Map;

import static org.junit.Assert.*;

public class FibonacciCalculatorTest {

    private final Map<Integer, BigInteger> positionExpectedValueMap
            = Map.of(0, BigInteger.valueOf(0L),
                     1, BigInteger.valueOf(1L),
                     10, BigInteger.valueOf(55L),
                     13, BigInteger.valueOf(233L),
                     20, BigInteger.valueOf(6765L),
                     50, BigInteger.valueOf(12586269025L)
    );

    private final FibonacciCalculator fibonacciCalculator = new FibonacciCalculator();


    @Test
    public void calculateFibonacci() {
        positionExpectedValueMap.forEach((position, expected) ->
                Assert.assertEquals(expected, fibonacciCalculator.calculateFibonacci(position))
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenNegativePositionThenIllegalArgumentException() {
        final int negativeNumber = -5;
        fibonacciCalculator.calculateFibonacci(negativeNumber);
    }
}