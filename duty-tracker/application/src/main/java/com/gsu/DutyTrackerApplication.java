package com.gsu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DutyTrackerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DutyTrackerApplication.class, args);
    }

}
