<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.gsu.presentation.constants.Constants" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="pathDuty" value="${contextPath}${Constants.PATH_DUTY}"/>
<!DOCTYPE html>
<html>
<c:set scope="request" var="title" value="Operation save"/>
<jsp:include page="${Constants.JSP_HEADER}"/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-6" >
            <h2 class="text-center">Duty</h2>
            <form:form method="post"
                       action="${pathDuty}${Constants.PATH_SAVE}"
                       modelAttribute="duty">
                <div class="form-group">
                    <label>Date</label>
                    <input type="date" name="date" value="${duty.date}" class="form-control"/>
                </div>
                <div class="form-group">
                    <label>Time(minutes)</label>
                    <form:input path="time" cssClass="form-control"/>
                </div>
                <div class="form-group">
                    <label>Parts count</label>
                    <form:input path="partsCount" cssClass="form-control"/>
                </div>
                <div class="form-group">
                    <label>Employee</label>
                    <form:select path="employeeId" items="${employees}" itemValue="id" itemLabel="fullName" />
                </div>
                <div class="form-group">
                    <label>Operation</label>
                    <form:select path="operationId" items="${operations}" itemValue="id" itemLabel="name" />
                </div>
                <div class="form-group">
                    <label>Workshop</label>
                    <form:select path="workshopId" items="${workshops}" itemValue="id" itemLabel="name" />
                </div>
                <form:hidden path="id" />
                <input type="submit" value="Save" class="btn btn-outline-success"/>
                <a href="${pathDuty}${Constants.PATH_ALL}"
                   class="btn btn-outline-danger">Back</a>
            </form:form>
        </>
    </div>
</div>
</html>