<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.gsu.presentation.constants.Constants" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="pathEmployee" value="${contextPath}${Constants.PATH_EMPLOYEE}"/>
<!DOCTYPE html>
<html>
<c:set scope="request" var="title" value="Employees"/>
<jsp:include page="${Constants.JSP_HEADER}"/>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a href="${pathEmployee}${Constants.PATH_SAVE_FORM}"
               class="btn btn-outline-success add-button">
                Add
            </a>
            <table class="table table-striped table-bordered" id="dataTable">
                <thead>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Age
                    </th>
                    <th>

                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${employees}" var="employee">
                    <tr>
                        <td>
                                ${employee.fullName}
                        </td>
                        <td>
                            ${employee.age}
                        </td>
                        <td>
                            <a href="${pathEmployee}${Constants.PATH_SAVE_FORM}?id=${employee.id}"
                               class="btn btn-outline-info">
                                Update
                            </a>
                            <form action="${pathEmployee}${Constants.PATH_DELETE}" method="post"
                                  class="delete-form">
                                <input type="hidden" name="id" value="${employee.id}"/>
                                <input type="submit" class="btn btn-outline-danger" value="Delete"/>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>