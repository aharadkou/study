<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.gsu.presentation.constants.Constants" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="pathWorkshop" value="${contextPath}${Constants.PATH_WORKSHOP}"/>
<!DOCTYPE html>
<html>
    <c:set scope="request" var="title" value="Workshop save"/>
    <jsp:include page="${Constants.JSP_HEADER}"/>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-6" >
                <h2 class="text-center">Workshop</h2>
                <form:form method="post"
                           action="${pathWorkshop}${Constants.PATH_SAVE}"
                           modelAttribute="workshop">
                    <div class="form-group">
                        <label>Name</label>
                        <form:input path="name" cssClass="form-control"/>
                    </div>
                    <form:hidden path="id" />
                    <input type="submit" value="Save" class="btn btn-outline-success"/>
                    <a href="${pathWorkshop}${Constants.PATH_ALL}"
                       class="btn btn-outline-danger">Back</a>
                </form:form>
            </div>
        </div>
    </div>
</html>
