<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.gsu.presentation.constants.Constants" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="pathWorkshop" value="${contextPath}${Constants.PATH_WORKSHOP}"/>
<!DOCTYPE html>
<html>
    <c:set scope="request" var="title" value="Workshops"/>
    <jsp:include page="${Constants.JSP_HEADER}"/>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <a href="${pathWorkshop}${Constants.PATH_SAVE_FORM}"
                       class="btn btn-outline-success add-button">
                        Add
                    </a>
                    <table class="table table-striped table-bordered" id="dataTable">
                        <thead>
                            <tr>
                                <th>
                                    Name
                                </th>
                                <th>

                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${workshops}" var="workshop">
                                <tr>
                                    <td>
                                        ${workshop.name}
                                    </td>
                                    <td>
                                        <a href="${pathWorkshop}${Constants.PATH_SAVE_FORM}?id=${workshop.id}"
                                           class="btn btn-outline-info">
                                            Update
                                        </a>
                                        <form action="${pathWorkshop}${Constants.PATH_DELETE}" method="post"
                                              class="delete-form">
                                            <input type="hidden" name="id" value="${workshop.id}"/>
                                            <input type="submit" class="btn btn-outline-danger" value="Delete"/>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>