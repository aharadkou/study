<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="resources" value="${contextPath}/resources"/>
<%@page import="com.gsu.presentation.constants.Constants" %>
<head>
    <title>${title}</title>
    <link href="${resources}/css/bootstrap.css" rel="stylesheet">
    <link href="${resources}/css/style.css" rel="stylesheet">
    <script src="${resources}/js/jquery.min.js"></script>
    <link href="${resources}/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="${resources}/js/jquery.dataTables.min.js"></script>
    <script src="${resources}/js/bootstrap.bundle.min.js"></script>
    <script src="${resources}/js/script.js"></script>
</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Duty tracker</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
            aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="${contextPath}${Constants.PATH_WORKSHOP}${Constants.PATH_ALL}">Workshops</a>
            <a class="nav-item nav-link" href="${contextPath}${Constants.PATH_OPERATION}${Constants.PATH_ALL}">Operations</a>
            <a class="nav-item nav-link" href="${contextPath}${Constants.PATH_EMPLOYEE}${Constants.PATH_ALL}">Employees</a>
            <a class="nav-item nav-link" href="${contextPath}${Constants.PATH_DUTY}${Constants.PATH_ALL}">Duties</a>
            <a class="nav-item nav-link" href="${contextPath}${Constants.PATH_REPORT}${Constants.PATH_DUTY}">Report</a>
        </div>
    </div>
</nav>