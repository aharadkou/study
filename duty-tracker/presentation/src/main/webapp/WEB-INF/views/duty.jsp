<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.gsu.presentation.constants.Constants" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="pathDuty" value="${contextPath}${Constants.PATH_DUTY}"/>
<!DOCTYPE html>
<html>
<c:set scope="request" var="title" value="Duties"/>
<jsp:include page="${Constants.JSP_HEADER}"/>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a href="${pathDuty}${Constants.PATH_SAVE_FORM}"
               class="btn btn-outline-success add-button">
                Add
            </a>
            <table class="table table-striped table-bordered" id="dataTable">
                <thead>
                <tr>
                    <th>
                        Date
                    </th>
                    <th>
                        Time(minutes)
                    </th>
                    <th>
                        Parts count
                    </th>
                    <th>
                        Employee
                    </th>
                    <th>
                        Operation
                    </th>
                    <th>
                        Workshop
                    </th>
                    <th>

                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${duties}" var="duty">
                    <tr>
                        <td>
                            ${duty.date}
                        </td>
                        <td>
                            ${duty.time}
                        </td>
                        <td>
                            ${duty.partsCount}
                        </td>
                        <td>
                            ${duty.employee.fullName}
                        </td>
                        <td>
                            ${duty.operation.name}
                        </td>
                        <td>
                            ${duty.workshop.name}
                        </td>
                        <td>
                            <a href="${pathDuty}${Constants.PATH_SAVE_FORM}?id=${duty.id}"
                               class="btn btn-outline-info">
                                Update
                            </a>
                            <form action="${pathDuty}${Constants.PATH_DELETE}" method="post"
                                  class="delete-form">
                                <input type="hidden" name="id" value="${duty.id}"/>
                                <input type="submit" class="btn btn-outline-danger" value="Delete"/>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
