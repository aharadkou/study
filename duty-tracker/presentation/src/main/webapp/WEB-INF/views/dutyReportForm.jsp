<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.gsu.presentation.constants.Constants" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<c:set scope="request" var="title" value="Report form"/>
<jsp:include page="${Constants.JSP_HEADER}"/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-6" >
            <h2 class="text-center">Duty report</h2>
            <form method="get" action="${contextPath}${Constants.PATH_REPORT}${Constants.PATH_DUTY_REPORT}">
                <div class="form-group">
                    <label>Enter start date</label>
                    <input type="date" name="startDate"/>
                </div>
                <div class="form-group">
                    <label>Enter end date</label>
                    <input type="date" name="endDate"/>
                </div>
                <input type="submit" value="Get report" class="btn btn-outline-success"/>
            </form>
        </div>
    </div>
</div>
</html>