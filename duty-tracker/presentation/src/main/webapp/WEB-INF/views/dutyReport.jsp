<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.gsu.presentation.constants.Constants" %>
<!DOCTYPE html>
<html>
<c:set scope="request" var="title" value="Duty report"/>
<jsp:include page="${Constants.JSP_HEADER}"/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-8" >
            <h2 class="text-center">Duties from ${param.startDate} to ${param.endDate}</h2>
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>
                        Date
                    </th>
                    <th>
                        Time(minutes)
                    </th>
                    <th>
                        Parts count
                    </th>
                    <th>
                        Employee
                    </th>
                    <th>
                        Operation
                    </th>
                    <th>
                        Workshop
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${duties}" var="duty">
                    <tr>
                        <td>
                                ${duty.date}
                        </td>
                        <td>
                                ${duty.time}
                        </td>
                        <td>
                                ${duty.partsCount}
                        </td>
                        <td>
                                ${duty.employee.fullName}
                        </td>
                        <td>
                                ${duty.operation.name}
                        </td>
                        <td>
                                ${duty.workshop.name}
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</html>
