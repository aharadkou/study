package com.gsu.presentation.controllers;

import com.gsu.persistence.data.Operation;
import com.gsu.persistence.services.interfaces.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static com.gsu.presentation.constants.Constants.*;
import static com.gsu.presentation.constants.Constants.PATH_ALL;

@Controller
@RequestMapping(PATH_OPERATION)
public class OperationController {

    private final OperationService operationService;

    @Value("#{servletContext.contextPath}")
    private String servletContextPath;

    @Autowired
    public OperationController(final OperationService operationService) {
        this.operationService = operationService;
    }

    @GetMapping(PATH_ALL)
    public String getAll(final Model model) {
        model.addAttribute("operations", operationService.findAll());
        return PAGE_OPERATION;
    }

    @GetMapping(PATH_SAVE_FORM)
    public String forwardToSaveForm(@RequestParam(required = false) final Long id,
                                    final Model model) {
        if (id != null) {
            model.addAttribute("operation", operationService.findById(id));
        } else {
            model.addAttribute("operation", new Operation());
        }
        return PAGE_OPERATION + SAVE_FORM_POSTFIX;
    }

    @PostMapping(PATH_SAVE)
    public String saveOperation(@ModelAttribute("operation") final Operation operation) {
        operationService.save(operation);
        return REDIRECT_PREFIX + PATH_OPERATION + PATH_ALL;
    }

    @PostMapping(PATH_DELETE)
    public String saveOperation(@RequestParam final Long id) {
        operationService.deleteById(id);
        return REDIRECT_PREFIX + PATH_OPERATION + PATH_ALL;
    }

}
