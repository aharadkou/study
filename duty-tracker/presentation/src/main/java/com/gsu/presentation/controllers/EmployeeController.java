package com.gsu.presentation.controllers;

import com.gsu.persistence.data.Employee;
import com.gsu.persistence.services.interfaces.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static com.gsu.presentation.constants.Constants.*;
import static com.gsu.presentation.constants.Constants.PAGE_EMPLOYEE;

@Controller
@RequestMapping(PATH_EMPLOYEE)
public class EmployeeController {

    private final EmployeeService employeeService;

    @Value("#{servletContext.contextPath}")
    private String servletContextPath;

    @Autowired
    public EmployeeController(final EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping(PATH_ALL)
    public String getAll(final Model model) {
        model.addAttribute("employees", employeeService.findAll());
        return PAGE_EMPLOYEE;
    }

    @GetMapping(PATH_SAVE_FORM)
    public String forwardToSaveForm(@RequestParam(required = false) final Long id,
                                    final Model model) {
        if (id != null) {
            model.addAttribute("employee", employeeService.findById(id));
        } else {
            model.addAttribute("employee", new Employee());
        }
        return PAGE_EMPLOYEE + SAVE_FORM_POSTFIX;
    }

    @PostMapping(PATH_SAVE)
    public String saveEmployee(@ModelAttribute("employee") final Employee employee) {
        employeeService.save(employee);
        return REDIRECT_PREFIX + PATH_EMPLOYEE + PATH_ALL;
    }

    @PostMapping(PATH_DELETE)
    public String saveEmployee(@RequestParam final Long id) {
        employeeService.deleteById(id);
        return REDIRECT_PREFIX + PATH_EMPLOYEE + PATH_ALL;
    }


}
