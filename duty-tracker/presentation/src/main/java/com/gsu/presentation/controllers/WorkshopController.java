package com.gsu.presentation.controllers;

import com.gsu.persistence.data.Workshop;
import com.gsu.persistence.services.interfaces.WorkshopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import static com.gsu.presentation.constants.Constants.*;

@Controller
@RequestMapping(PATH_WORKSHOP)
public class WorkshopController {

    private final WorkshopService workshopService;

    @Value("#{servletContext.contextPath}")
    private String servletContextPath;

    @Autowired
    public WorkshopController(final WorkshopService workshopService) {
        this.workshopService = workshopService;
    }

    @GetMapping(PATH_ALL)
    public String getAll(final Model model) {
        model.addAttribute("workshops", workshopService.findAll());
        return PAGE_WORKSHOP;
    }

    @GetMapping(PATH_SAVE_FORM)
    public String forwardToSaveForm(@RequestParam(required = false) final Long id,
                                    final Model model) {
        if (id != null) {
           model.addAttribute("workshop", workshopService.findById(id));
        } else {
            model.addAttribute("workshop", new Workshop());
        }
        return PAGE_WORKSHOP + SAVE_FORM_POSTFIX;
    }

    @PostMapping(PATH_SAVE)
    public String saveWorkshop(@ModelAttribute("workshop") final Workshop workshop) {
        workshopService.save(workshop);
        return REDIRECT_PREFIX + PATH_WORKSHOP + PATH_ALL;
    }

    @PostMapping(PATH_DELETE)
    public String saveWorkshop(@RequestParam final Long id) {
        workshopService.deleteById(id);
        return REDIRECT_PREFIX + PATH_WORKSHOP + PATH_ALL;
    }

}
