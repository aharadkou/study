package com.gsu.presentation.controllers;

import com.gsu.persistence.data.Duty;
import com.gsu.persistence.services.interfaces.DutyService;
import com.gsu.persistence.services.interfaces.EmployeeService;
import com.gsu.persistence.services.interfaces.OperationService;
import com.gsu.persistence.services.interfaces.WorkshopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

import static com.gsu.presentation.constants.Constants.*;

@Controller
@RequestMapping(PATH_DUTY)
public class DutyController {

    private final DutyService dutyService;

    private final OperationService operationService;

    private final WorkshopService workshopService;

    private final EmployeeService employeeService;

    @Autowired
    public DutyController(final DutyService dutyService,
                          final OperationService operationService,
                          final WorkshopService workshopService,
                          final EmployeeService employeeService) {
        this.dutyService = dutyService;
        this.operationService = operationService;
        this.workshopService = workshopService;
        this.employeeService = employeeService;
    }

    @GetMapping(PATH_ALL)
    public String getAll(final Model model) {
        model.addAttribute("duties", dutyService.findAll());
        return PAGE_DUTY;
    }

    @GetMapping(PATH_SAVE_FORM)
    public String forwardToSaveForm(@RequestParam(required = false) final Long id,
                                    final Model model) {
        if (id != null) {
            model.addAttribute("duty", dutyService.findById(id));
        } else {
            var duty = new Duty();
            duty.setDate(new Date(new java.util.Date().getTime()));
            model.addAttribute("duty", duty);
        }
        model.addAttribute("employees", employeeService.findAll());
        model.addAttribute("operations", operationService.findAll());
        model.addAttribute("workshops", workshopService.findAll());
        return PAGE_DUTY + SAVE_FORM_POSTFIX;
    }

    @PostMapping(PATH_SAVE)
    public String saveDuty(@ModelAttribute("duty") final Duty duty) {
        dutyService.save(duty);
        return REDIRECT_PREFIX + PATH_DUTY + PATH_ALL;
    }

    @PostMapping(PATH_DELETE)
    public String saveDuty(@RequestParam final Long id) {
        dutyService.deleteById(id);
        return REDIRECT_PREFIX + PATH_DUTY + PATH_ALL;
    }

}
