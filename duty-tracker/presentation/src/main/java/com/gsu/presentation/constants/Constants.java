package com.gsu.presentation.constants;

public class Constants {

    public static final String REDIRECT_PREFIX = "redirect:";

    public static final String SAVE_FORM_POSTFIX = "Save";

    public static final String PAGE_DUTY = "duty";

    public static final String PATH_DUTY = "/" + PAGE_DUTY;

    public static final String PAGE_EMPLOYEE = "employee";

    public static final String PATH_EMPLOYEE = "/" + PAGE_EMPLOYEE;

    public static final String PAGE_OPERATION = "operation";

    public static final String PATH_OPERATION = "/" + PAGE_OPERATION;

    public static final String PAGE_WORKSHOP = "workshop";

    public static final String PATH_WORKSHOP = "/" + PAGE_WORKSHOP;

    public static final String PATH_ALL = "/all";

    public static final String PATH_SAVE = "/save";

    public static final String PATH_SAVE_FORM = "/saveForm";

    public static final String PATH_DELETE = "/delete";


    public static final String PATH_REPORT = "/report";

    public static final String PATH_DUTY_REPORT = PATH_DUTY + "/date";

    public static final String JSP_HEADER = "/WEB-INF/views/_header.jsp";
}
