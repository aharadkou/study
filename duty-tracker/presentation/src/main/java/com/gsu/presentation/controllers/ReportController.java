package com.gsu.presentation.controllers;

import com.gsu.persistence.services.interfaces.DutyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;

import static com.gsu.presentation.constants.Constants.*;

@Controller
@RequestMapping(PATH_REPORT)
public class ReportController {

    private final DutyService dutyService;

    @Autowired
    public ReportController(final DutyService dutyService) {
        this.dutyService = dutyService;
    }

    @GetMapping(PATH_DUTY)
    public String forwardToReportForm() {
        return "dutyReportForm";
    }

    @GetMapping(PATH_DUTY_REPORT)
    public String getReport(@RequestParam final Date startDate,
                            @RequestParam final Date endDate,
                            final Model model) {
        model.addAttribute("duties", dutyService.findAllByDateRange(startDate, endDate));
        return "dutyReport";
    }

}
