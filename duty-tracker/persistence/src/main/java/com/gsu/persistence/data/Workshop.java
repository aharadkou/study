package com.gsu.persistence.data;

import lombok.Data;

import javax.persistence.*;

/**
 * Represents a workshop.
 */
@Data
@Entity
public class Workshop {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;

}
