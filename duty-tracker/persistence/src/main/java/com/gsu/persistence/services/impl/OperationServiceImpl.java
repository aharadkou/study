package com.gsu.persistence.services.impl;

import com.gsu.persistence.data.Operation;
import com.gsu.persistence.services.interfaces.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class OperationServiceImpl extends AbstractCrudServiceImpl<Operation> implements OperationService {

    @Autowired
    public OperationServiceImpl(JpaRepository<Operation, Long> repository) {
        super(repository);
    }
}
