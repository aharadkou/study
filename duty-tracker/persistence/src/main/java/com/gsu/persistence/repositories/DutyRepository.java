package com.gsu.persistence.repositories;

import com.gsu.persistence.data.Duty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface DutyRepository extends JpaRepository<Duty, Long> {

    List<Duty> findAllByDateBetween(Date startDate, Date endDate);

}
