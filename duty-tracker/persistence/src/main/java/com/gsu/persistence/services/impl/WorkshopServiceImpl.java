package com.gsu.persistence.services.impl;

import com.gsu.persistence.data.Workshop;
import com.gsu.persistence.services.interfaces.WorkshopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class WorkshopServiceImpl extends AbstractCrudServiceImpl<Workshop> implements WorkshopService {

    @Autowired
    public WorkshopServiceImpl(JpaRepository<Workshop, Long> repository) {
        super(repository);
    }
}
