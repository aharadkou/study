package com.gsu.persistence.services.interfaces;

import com.gsu.persistence.data.Workshop;

public interface WorkshopService extends CrudService<Workshop> {

}
