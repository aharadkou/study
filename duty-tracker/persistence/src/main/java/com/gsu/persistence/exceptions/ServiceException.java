package com.gsu.persistence.exceptions;

public class ServiceException extends RuntimeException {

    public ServiceException(final String mes) {
        super(mes);
    }

}
