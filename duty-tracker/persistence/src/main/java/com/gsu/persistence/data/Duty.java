package com.gsu.persistence.data;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

/**
 * Represents a duty.
 */
@Data
@Entity
public class Duty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date date;

    private int time;

    private int partsCount;

    @Column(name = "employee_id")
    private Long employeeId;

    @ManyToOne
    @JoinColumn(name = "employee_id", insertable = false, updatable = false)
    private Employee employee;

    @Column(name = "operation_id")
    private Long operationId;

    @ManyToOne
    @JoinColumn(name = "operation_id", insertable = false, updatable = false)
    private Operation operation;

    @Column(name = "workshop_id")
    private Long workshopId;

    @ManyToOne
    @JoinColumn(name = "workshop_id", insertable = false, updatable = false)
    private Workshop workshop;

}
