package com.gsu.persistence.services.interfaces;

import com.gsu.persistence.data.Duty;

import java.sql.Date;
import java.util.List;

public interface DutyService extends CrudService<Duty> {

    List<Duty> findAllByDateRange(Date startDate, Date endDate);

}
