package com.gsu.persistence.services.interfaces;

import com.gsu.persistence.data.Operation;

public interface OperationService extends CrudService<Operation> {

}
