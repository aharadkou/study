package com.gsu.persistence.services.interfaces;

import com.gsu.persistence.data.Employee;

public interface EmployeeService extends CrudService<Employee> {

}
