package com.gsu.persistence.services.interfaces;

import java.util.List;

public interface CrudService<T> {

    List<T> findAll();

    T findById(Long id);

    T save(T entity);

    void deleteById(Long id);

}
