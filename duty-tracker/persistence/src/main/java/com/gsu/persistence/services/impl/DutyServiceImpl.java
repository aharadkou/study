package com.gsu.persistence.services.impl;

import com.gsu.persistence.data.Duty;
import com.gsu.persistence.repositories.DutyRepository;
import com.gsu.persistence.services.interfaces.DutyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class DutyServiceImpl extends AbstractCrudServiceImpl<Duty> implements DutyService {

    private final DutyRepository dutyRepository;

    @Autowired
    protected DutyServiceImpl(DutyRepository dutyRepository) {
        super(dutyRepository);
        this.dutyRepository = dutyRepository;
    }

    @Override
    public List<Duty> findAllByDateRange(Date startDate, Date endDate) {
        return dutyRepository.findAllByDateBetween(startDate, endDate);
    }
}

