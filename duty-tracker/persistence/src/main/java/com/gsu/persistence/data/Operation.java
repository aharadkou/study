package com.gsu.persistence.data;

import lombok.Data;

import javax.persistence.*;

/**
 * Represents an operation.
 */
@Data
@Entity
public class Operation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String name;

}
