package com.gsu.persistence.services.impl;

import com.gsu.persistence.data.Employee;
import com.gsu.persistence.services.interfaces.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends AbstractCrudServiceImpl<Employee> implements EmployeeService {


    @Autowired
    public EmployeeServiceImpl(JpaRepository<Employee, Long> employeeRepository) {
        super(employeeRepository);
    }
}
