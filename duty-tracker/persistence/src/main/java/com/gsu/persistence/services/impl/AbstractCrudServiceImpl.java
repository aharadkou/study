package com.gsu.persistence.services.impl;

import com.gsu.persistence.exceptions.ServiceException;
import com.gsu.persistence.services.interfaces.CrudService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract class AbstractCrudServiceImpl<T> implements CrudService<T> {

    private final JpaRepository<T, Long> repository;

    protected AbstractCrudServiceImpl(final JpaRepository<T, Long> repository) {
        this.repository = repository;
    }

    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public T findById(final Long id) {
        return repository.findById(id).orElseThrow(() -> new ServiceException("Entity with such id not found!"));
    }

    @Override
    public T save(final T entity) {
        return repository.save(entity);
    }

    @Override
    public void deleteById(final Long id) {
        try {
            repository.deleteById(id);
        } catch (DataIntegrityViolationException ex) {
            throw new ServiceException("Entity deletion problem!");
        }
    }
}
