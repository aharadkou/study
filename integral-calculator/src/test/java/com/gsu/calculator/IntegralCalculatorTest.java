package com.gsu.calculator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class IntegralCalculatorTest {

    @Autowired
    private IntegralCalculatorImpl integralCalculator;

    @Test
    public void calculate() {
        //given
        final double intervalStart = 1.0;
        final double intervalEnd = 10.0;
        final double precision = 0.01;
        final double expected = 7.0275308934736;
        //when
        double actual = integralCalculator.calculate(intervalStart, intervalEnd, precision);
        //then
        Assert.assertTrue(actual >= expected - precision && actual <= expected + precision);
    }

    @Test
    public void calculateExtreme() {
        //given
        final double intervalStart = 0.001;
        final double intervalEnd = 50.0;
        final double precision = 0.0001;
        final double expected = -1312.5638864012387;
        //when
        double actual = integralCalculator.calculate(intervalStart, intervalEnd, precision);
        //then
        Assert.assertTrue(actual >= expected - precision && actual <= expected + precision);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenIntervalEndLessThanIntervalStart_thenIllegalArgumentException() {
        integralCalculator.calculate(5, 2, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenPrecisionIsNegative_thenIllegalArgumentException() {
        integralCalculator.calculate(5, 2, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenIntervalStartIsNegative_thenIllegalArgumentException() {
        integralCalculator.calculate(-5, 2, 1);
    }

}
