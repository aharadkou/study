package com.gsu.dto;

import lombok.Data;

@Data
public class RequestDto {

    private double startInterval;

    private double endInterval;

    private double precision;

}
