package com.gsu.calculator;

import org.springframework.stereotype.Component;

import static java.lang.Math.*;

@Component
public class IntegralCalculatorImpl implements IntegralCalculator {

    private final double MAX_CALCULATE_PRECISION = 0.00001;

    public double calculate(final double intervalStart, final double intervalEnd, final double precision) {
        validateInput(intervalStart, intervalEnd, precision);
        int intervalCount = calculateIntervalCount(intervalStart, intervalEnd, precision);
        double step = (intervalEnd - intervalStart) / intervalCount;
        double intervalStartValue = calculateFunctionValue(intervalStart);
        double intervalEndValue = calculateFunctionValue(intervalEnd);
        double current = intervalStart + step;
        double sum = intervalStartValue + intervalEndValue;
        while(current <= intervalEnd - step) {
            sum += calculateFunctionValue(current) * 2.0;
            current += step;
        }
        return (step / 2.0) * sum;
    }

    private void validateInput(final double intervalStart, final double intervalEnd, final double precision) {
        if (intervalStart <= 0) {
            throw new IllegalArgumentException("Interval start must be greater than 0!");
        }
        if (intervalEnd <= intervalStart) {
            throw new IllegalArgumentException("Interval start must be less than interval end!");
        }
        if (precision <= 0) {
            throw new IllegalArgumentException("Precision must be greater than 0!");
        }
    }

    private double calculateFunctionValue(final double x) {
        return (log(x) * log(x) * log(x)) / x;
    }

    private int calculateIntervalCount(final double intervalStart, final double intervalEnd, final double precision) {
        double intervalCount = sqrt(abs(calculateFunctionMax(intervalStart, intervalEnd)
                * pow(intervalEnd - intervalStart, 3.0) / 12 / precision));
        return (int)ceil(intervalCount);
    }

    private double calculateFunctionMax(final double intervalStart, final double intervalEnd) {
        double current = intervalStart;
        double max = - Double.MAX_VALUE;
        while (current <= intervalEnd) {
            double currentValue = (6.0 + 2.0 * log(current)* log(current) - 9.0 * log(current)) * log(current)
                                        / (current * current * current);
            if (max < currentValue) {
                max = currentValue;
            }
            current += MAX_CALCULATE_PRECISION;
        }
        return max;
    }
}
