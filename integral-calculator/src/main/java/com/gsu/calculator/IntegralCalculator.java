package com.gsu.calculator;

public interface IntegralCalculator {

    double calculate(double intervalStart, double intervalEnd, double precision);

}
