package com.gsu.contollers;

import com.gsu.calculator.IntegralCalculator;
import com.gsu.dto.RequestDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/integral")
@CrossOrigin("http://localhost:63342")
public class IntegralController {

    private final IntegralCalculator integralCalculator;

    public IntegralController(final IntegralCalculator integralCalculator) {
        this.integralCalculator = integralCalculator;
    }

    @PostMapping
    public double calculateIntegral(@RequestBody final RequestDto requestDto) {
        try {
            return integralCalculator.calculate(requestDto.getStartInterval(),
                    requestDto.getEndInterval(), requestDto.getPrecision());
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        }
    }

}
