$(document).ready(function() {

    var ctx = document.getElementById("chart");
    var labels = [];
    var start = 0.01;
    var end = 20;
    var precision = 0.01;
    var current = start;
    while(current <= end) {
        labels.push(current.toFixed(2));
        current += precision;
    }
    var data = {
        labels : labels,
        datasets: [{
            label: "ln(x) ^ 3 / x",
            function: function(x) {
                if(x === 0) {
                    return 0;
                }
                return Math.log(x) * Math.log(x) * Math.log(x) / x;
            },
            data: [],
            borderColor: "rgba(75, 192, 192, 1)",
            fill: false
        }]
    };
    Chart.pluginService.register({
        beforeInit: function(chart) {
            var data = chart.config.data;
            for (var i = 0; i < data.datasets.length; i++) {
                for (var j = 0; j < data.labels.length; j++) {
                    var fct = data.datasets[i].function,
                        x = data.labels[j],
                        y = fct(x);
                    data.datasets[i].data.push(y);
                }
            }
        }
    });
    var chart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: {

            scaleBeginAtZero: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:false,
                        min : -10,
                        max : 10
                    }
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 20

                    }
                }]
            }
        }
    });

    $('#calculateIntegral').click(function () {
        var data = {"startInterval":$("#startInterval").val(),
                    "endInterval":$("#endInterval").val(),
                    "precision":$("#precision").val()};
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/integral",
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            success: function(data) {
                $("#result").text(data);
                $("#startInterval").val("");
                $("#endInterval").val("");
                $("#precision").val("")
            },
            error: function (xhr, status, errorThrown) {
                alert(JSON.parse(xhr.responseText).message);
            }
        });
    });

});